Feature: Get current round information

  As a user I want to know the current round information

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "special": true,
            "rewards": [
              {
                "code": "gold",
                "amount": 10
              },
              {
                "code": "gems",
                "amount": 20
              }
            ]
          },
          {
            "number": 2,
            "special": false,
            "rewards": [
              {
                "code": "gold",
                "amount": 20
              }
            ]
          }
        ]
      }
      """

  Scenario: Get current round information
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    When client call route 'rubypitaya.towerOfFortuneHandler.getCurrentRoundInfo'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "number": 1,
          "special": true,
          "numberOfRewards": 2
        }
      }
      """

  Scenario: Can't get current round if is not playing a game
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    When  client call route 'rubypitaya.towerOfFortuneHandler.getCurrentRoundInfo'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You are not on a game to get current round info"
      }
      """

  Scenario: Can't get current round if has failed
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   user '00000000-0000-0000-0000-000000000001' has failed a round
    When  client call route 'rubypitaya.towerOfFortuneHandler.getCurrentRoundInfo'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You have failed, continue game to get current round info"
      }
      """