Feature: Start Tower of Fortune Game

  As a user I want to start playing tower of fortune

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/cost_info": {
          "startGameCost": 10,
          "continueGameCost": [1, 2, 3, 4, 5],
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        }
      }
      """

  Scenario: Get tower of fortune info when user not started a game
    Given client call route 'rubypitaya.towerOfFortuneHandler.getTowerInfo'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "playedGamesCount": 0,
          "isPlayingGame": false,
          "hasFreeGame": true,
          "unixFreeGameAt": 1609459200,
          "startGameCost": 10,
          "continueGameCost": 1,
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        }
      }
      """

  Scenario: Start free tower of fortune game
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "cost": 0,
          "currencyCode": "gems",
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          }
        }
      }
      """

  Scenario: Can't start tower of fortune if feature is not enabled
    Given config is the following json:
      """
      { "plugin/tower_of_fortune/feature": { "enabled": false } }
      """
    And   client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    Then  server should response the following json:
      """
      { "code": "RP-200", "message": "Tower of Fortune is not enabled", "data": {} }
      """

  Scenario: Can't start tower of fortune game by not enough currency
    Given the following Tower of Fortune:
      | user_id                              | free_game_at        |
      | 00000000-0000-0000-0000-000000000001 | 2021-01-02 00:00:00 |
    And   user don't has currency to start a Tower of Fortune Game
    And   client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "Not enough currency"
      }
      """