Given(/^[Tt]he following [Tt]ower [Oo]f [Ff]ortune[s]*[:]*$/) do |table|
  tower_hashes = table.hashes
  tower_hashes.each do |tower_hash|
    tower_hash[:user] = get_user(tower_hash[:user_id])
    tower_hash[:free_game_at] ||= Time.now.utc
    TowerOfFortune::TowerOfFortune.create(tower_hash)
  end
end

Given(/^[Uu]ser don[']?t has currency to start a [Tt]ower [Oo]f [Ff]ortune [Gg]ame$/) do
  get_objects[:bll].stub(:has_currency_to_start_new_game?) do |user_id, cost, currency_code, tower_info, game_info|
    false
  end
end

Given(/^[Uu]ser don[']?t has currency to continue after fail a [Tt]ower [Oo]f [Ff]ortune [Gg]ame$/) do
  get_objects[:bll].stub(:has_currency_to_continue_game_after_fail?) do |user_id, cost, currency_code, tower_info, game_info|
    false
  end
end

Given(/^[Nn]ext round option reward index is ["'](.+)["']$/) do |index_text|
  index = index_text.to_i
  get_objects[:bll].stub(:choose_reward_index) do |user_id, rewards, tower_info, game_info|
    index
  end
end

Given(/^[Ww]ill fail next round$/) do
  get_objects[:bll].stub(:has_lost_round?) do |user_id, reward, tower_info, game_info|
    true
  end
end

Given(/^[Uu]ser ["'](.+)["'] already won the final round$/) do |user_id|
  tower = get_tower(user_id)
  tower.game_info.won_final_round = true
  tower.save
end

Given(/^[Uu]ser ["'](.+)["'] has failed a round$/) do |user_id|
  tower = get_tower(user_id)
  tower.game_info.has_failed = true
  tower.save
end

def get_user(user_id)
  user = User.find_by_id(user_id)
  user = User.create(id: user_id) if user.nil?
  user
end

def get_tower(user_id)
    tower = TowerOfFortune::TowerOfFortune.find_by_user_id(user_id)
    tower = TowerOfFortune::TowerOfFortune.create(user_id: user_id) if tower.nil?
    tower
end

def get_objects
  TowerOfFortune::TowerOfFortuneHandler.objects
end