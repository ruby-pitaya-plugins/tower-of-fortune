Feature: Continue game after fail

  As a user I want to continue game after fail a round

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/cost_info": {
          "startGameCost": 10,
          "continueGameCost": [1, 2, 3, 4, 5],
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "rewards": [
              {
                "code": "reward-001",
                "amount": 10
              }
            ]
          },
          {
            "number": 2,
            "rewards": [
              {
                "code": "reward-002",
                "amount": 20
              }
            ]
          }
        ]
      }
      """

  Scenario: Continue game after fail round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    And   client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   will fail next round
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "cost": 1,
          "currencyCode": "gems",
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 1,
            "currentRound": 1,
            "wonFinalRound": false,
            "accumulatedRewards": []
          }
        }
      }
      """

  Scenario: Continue game two times after fail round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    And   client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   will fail next round
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    And   will fail next round
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "cost": 2,
          "currencyCode": "gems",
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 2,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 2,
            "currentRound": 1,
            "wonFinalRound": false,
            "accumulatedRewards": []
          }
        }
      }
      """

  Scenario: Can't continue game after fail if is not playing a game
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    When  client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You are not on a game to continue it"
      }
      """

  Scenario: Can't continue game after fail if has not failed
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    When  client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You have not failed to continue a game"
      }
      """

  Scenario: Can't continue game if has not enough currency
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    And   client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   will fail next round
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   user don't has currency to continue after fail a Tower of Fortune Game
    When  client call route 'rubypitaya.towerOfFortuneHandler.continueGameAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "Not enough currency"
      }
      """
