Feature: Collect tower of fortune accumulated rewards

  As a user I want to collect my accumulated rewards

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/cost_info": {
          "startGameCost": 10,
          "continueGameCost": [1, 2, 3, 4, 5],
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "rewards": [
              {
                "code": "reward-001",
                "amount": 10
              },
              {
                "code": "reward-002",
                "amount": 20
              }
            ]
          },
          {
            "number": 2,
            "rewards": [
              {
                "code": "reward-003",
                "amount": 30
              }
            ]
          }
        ]
      }
      """

  Scenario: Collect reward after receive it choosing a round option
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   next round option reward index is '0'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.collectRewards'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": false,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 2,
            "wonFinalRound": false,
            "accumulatedRewards": []
          },
          "rewards": [
            {
              "code": "reward-001",
              "amount": 10
            }
          ]
        }
      }
      """

  Scenario: Collect reward after finish final round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   next round option reward index is '1'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   next round option reward index is '0'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.collectRewards'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": false,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 2,
            "wonFinalRound": true,
            "accumulatedRewards": []
          },
          "rewards": [
            {
              "code": "reward-002",
              "amount": 20
            },
            {
              "code": "reward-003",
              "amount": 30
            }
          ]
        }
      }
      """

  Scenario: Can't collect reward if tower of fortune game has not started
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    When  client call route 'rubypitaya.towerOfFortuneHandler.collectRewards'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You are not on a game to collect your rewards"
      }
      """

  Scenario: Can't collect reward if has failed
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   user '00000000-0000-0000-0000-000000000001' has failed a round
    When  client call route 'rubypitaya.towerOfFortuneHandler.collectRewards'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You have failed, continue the game to collect your rewards"
      }
      """