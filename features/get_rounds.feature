Feature: Get rounds informations

  As a user I want to know the rounds on tower of fortune

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "special": true,
            "rewards": [
              {
                "code": "gold",
                "amount": 10
              },
              {
                "code": "gems",
                "amount": 20
              }
            ]
          },
          {
            "number": 2,
            "special": false,
            "rewards": [
              {
                "code": "gold",
                "amount": 20
              }
            ]
          }
        ]
      }
      """

  Scenario: Get current round information
    Given client call route 'rubypitaya.towerOfFortuneHandler.getRounds'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "rounds": [
            {
              "number": 1,
              "special": true,
              "numberOfRewards": 2
            },
            {
              "number": 2,
              "special": false,
              "numberOfRewards": 1
            }
          ],
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 1,
            "wonFinalRound": false,
            "accumulatedRewards": []
          }
        }
      }
      """

  Scenario: Can't get rounds if feature is not enabled
    Given config is the following json:
      """
      { "plugin/tower_of_fortune/feature": { "enabled": false } }
      """
    And   client call route 'rubypitaya.towerOfFortuneHandler.getRounds'
    Then  server should response the following json:
      """
      { "code": "RP-200", "message": "Tower of Fortune is not enabled", "data": {} }
      """