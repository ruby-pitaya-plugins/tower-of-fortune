Feature: Choose round option

  As a user I want to choose an option on round to try get a reward

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/cost_info": {
          "startGameCost": 10,
          "continueGameCost": [1, 2, 3, 4, 5],
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "rewards": [
              {
                "code": "reward-001",
                "amount": 10
              },
              {
                "code": "reward-002",
                "amount": 20
              },
              {
                "code": "reward-003",
                "amount": 30
              }
            ]
          },
          {
            "number": 2,
            "rewards": [
              {
                "code": "reward-001",
                "amount": 20
              },
              {
                "code": "reward-002",
                "amount": 30
              },
              {
                "code": "reward-003",
                "amount": 40
              }
            ]
          }
        ]
      }
      """

  Scenario: Choose a round option and accumulate a reward
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   next round option reward index is '0'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 2,
            "wonFinalRound": false,
            "accumulatedRewards": [
              {
                "code": "reward-001",
                "amount": 10
              }
            ]
          },
          "roundRewardInfo": {
            "chosenReward": {
              "code": "reward-001",
              "amount": 10
            },
            "otherRewards": [
              {
                "code": "reward-002",
                "amount": 20
              },
              {
                "code": "reward-003",
                "amount": 30
              }
            ]
          }
        }
      }
      """

  Scenario: Accumulate same reward at second round
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   next round option reward index is '0'
    And  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   next round option reward index is '0'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 2,
            "wonFinalRound": true,
            "accumulatedRewards": [
              {
                "code": "reward-001",
                "amount": 30
              }
            ]
          },
          "roundRewardInfo": {
            "chosenReward": {
              "code": "reward-001",
              "amount": 20
            },
            "otherRewards": [
              {
                "code": "reward-002",
                "amount": 30
              },
              {
                "code": "reward-003",
                "amount": 40
              }
            ]
          }
        }
      }
      """

  Scenario: Get tower of fortune game info after play a game round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   next round option reward index is '1'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.getGameInfo'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "hasFailed": false,
          "failCount": 0,
          "currentRound": 2,
          "wonFinalRound": false,
          "accumulatedRewards": [
            {
              "code": "reward-002",
              "amount": 20
            }
          ]
        }
      }
      """

  Scenario: Choose a round option and fail
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   next round option reward index is '0'
    And   will fail next round
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": true,
            "failCount": 1,
            "currentRound": 1,
            "wonFinalRound": false,
            "accumulatedRewards": []
          },
          "roundRewardInfo": {
            "chosenReward": {
              "code": "reward-001",
              "amount": 10
            },
            "otherRewards": [
              {
                "code": "reward-002",
                "amount": 20
              },
              {
                "code": "reward-003",
                "amount": 30
              }
            ]
          }
        }
      }
      """

  Scenario: Fail on second round
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   next round option reward index is '0'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   next round option reward index is '1'
    And   will fail next round
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": true,
            "failCount": 1,
            "currentRound": 2,
            "wonFinalRound": false,
            "accumulatedRewards": [
              {
                "code": "reward-001",
                "amount": 10
              }
            ]
          },
          "roundRewardInfo": {
            "chosenReward": {
              "code": "reward-002",
              "amount": 30
            },
            "otherRewards": [
              {
                "code": "reward-001",
                "amount": 20
              },
              {
                "code": "reward-003",
                "amount": 40
              }
            ]
          }
        }
      }
      """

  Scenario: Win last round
    Given client call route 'rubypitaya.towerOfFortuneHandler.startGame'
    And   next round option reward index is '0'
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    And   next round option reward index is '1'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": true,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": false,
            "failCount": 0,
            "currentRound": 2,
            "wonFinalRound": true,
            "accumulatedRewards": [
              {
                "code": "reward-001",
                "amount": 10
              },
              {
                "code": "reward-002",
                "amount": 30
              }
            ]
          },
          "roundRewardInfo": {
            "chosenReward": {
              "code": "reward-002",
              "amount": 30
            },
            "otherRewards": [
              {
                "code": "reward-001",
                "amount": 20
              },
              {
                "code": "reward-003",
                "amount": 40
              }
            ]
          }
        }
      }
      """

  Scenario: Can't choose a round if is not playing a game
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    And   next round option reward index is '0'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You are not on a game to choose an option"
      }
      """

  Scenario: Can't choose a round if already won final round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   user '00000000-0000-0000-0000-000000000001' already won the final round
    And   next round option reward index is '0'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You already won the final round, collect your prizes"
      }
      """

  Scenario: Can't choose a round if has failed
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   user '00000000-0000-0000-0000-000000000001' has failed a round
    And   next round option reward index is '0'
    When  client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You already fail, choose give up or continue"
      }
      """
