Feature: Give up after fail

  As a user I want to give up after fail a round

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And   the user '00000000-0000-0000-0000-000000000001' is authenticated
    And   today is '2021-01-01 00:00:00'
    And   config is the following json:
      """
      {
        "plugin/tower_of_fortune/feature": {
          "enabled": true
        },
        "plugin/tower_of_fortune/cost_info": {
          "startGameCost": 10,
          "continueGameCost": [1, 2, 3, 4, 5],
          "startGameCurrencyCode": "gems",
          "continueGameCurrencyCode": "gems"
        },
        "plugin/tower_of_fortune/rounds": [
          {
            "number": 1,
            "rewards": [
              {
                "code": "reward-001",
                "amount": 10
              }
            ]
          },
          {
            "number": 2,
            "rewards": [
              {
                "code": "reward-002",
                "amount": 20
              }
            ]
          }
        ]
      }
      """

  Scenario: Give up after fail round
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    And   will fail next round
    And   client call route 'rubypitaya.towerOfFortuneHandler.chooseRoundOption'
    When  client call route 'rubypitaya.towerOfFortuneHandler.giveUpAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "towerInfo": {
            "playedGamesCount": 1,
            "isPlayingGame": false,
            "hasFreeGame": false,
            "unixFreeGameAt": 1609545600,
            "startGameCost": 10,
            "continueGameCost": 1,
            "startGameCurrencyCode": "gems",
            "continueGameCurrencyCode": "gems"
          },
          "gameInfo": {
            "hasFailed": true,
            "failCount": 1,
            "currentRound": 1,
            "wonFinalRound": false,
            "accumulatedRewards": []
          }
        }
      }
      """

  Scenario: Can't give up if is not playing a game
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | false           |
    When  client call route 'rubypitaya.towerOfFortuneHandler.giveUpAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You are not on a game to give up"
      }
      """

  Scenario: Can't choose a round if has not failed
    Given the following Tower of Fortune:
      | user_id                              | is_playing_game |
      | 00000000-0000-0000-0000-000000000001 | true            |
    When  client call route 'rubypitaya.towerOfFortuneHandler.giveUpAfterFail'
    Then  server should response the following json:
      """
      {
        "code": "RP-201",
        "message": "You have not failed to give up"
      }
      """

  Scenario: Can't give up after fail if feature is not enabled
    Given config is the following json:
      """
      { "plugin/tower_of_fortune/feature": { "enabled": false } }
      """
    And   client call route 'rubypitaya.towerOfFortuneHandler.giveUpAfterFail'
    Then  server should response the following json:
      """
      { "code": "RP-200", "message": "Tower of Fortune is not enabled", "data": {} }
      """