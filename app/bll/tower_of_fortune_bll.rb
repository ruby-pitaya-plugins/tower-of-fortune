module TowerOfFortune

  class TowerOfFortuneBLL

    # parameter:  tower_info = {
    #               playedGamesCount:         [int],
    #               isPlayingGame:            [bool],
    #               hasFreeGame:              [bool],
    #               unixFreeGameAt:           [int],
    #               startGameCost:            [int],
    #               continueGameCost:         [int],
    #               startGameCurrencyCode:    [string],
    #               continueGameCurrencyCode: [string],
    #             }

    # parameter:  game_info = {
    #               hasFailed:          [bool],
    #               failCount:          [int],
    #               currentRound:       [int],
    #               wonFinalRound:      [int],
    #               accumulatedRewards: [int],
    #             }

    # parameter:  reward = {
    #               code:       [string],
    #               amount:     [int],
    #               [others fields on config file]
    #             }

    # parameter:  cost          = [int]
    # parameter:  currency_code = [string]
    # parameter:  user_id       = [int]

    def error_not_enough_currency
      return RubyPitaya::RouteError.new(StatusCodes::CODE_ERROR, 'Not enough currency')
    end

    def has_currency_to_start_new_game?(user_id, cost, currency_code, tower_info, game_info)
      return true
    end

    def apply_cost_to_start_new_game(user_id, cost, currency_code, tower_info, game_info)
    end

    def has_currency_to_continue_game_after_fail?(user_id, cost, currency_code, tower_info, game_info)
      return true
    end

    def apply_cost_to_continue_game_after_fail(user_id, cost, currency_code, tower_info, game_info)
    end

    def has_lost_round?(user_id, reward, tower_info, game_info)
      return false
    end

    def get_correct_round_reward(user_id, reward, tower_info, game_info)
      return reward
    end

    def choose_reward_index(user_id, rewards, tower_info, game_info)
      return rand(rewards.size)
    end

    def before_claim_rewards(user_id)
    end

    def claim_reward(user_id, reward, tower_info, game_info)
      return [reward]
    end

    def after_claim_rewards(user_id)
    end

    def gaveUp(user_id, tower_info, game_info)
    end
  end
end