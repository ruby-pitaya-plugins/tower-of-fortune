require 'active_record'

class CreateTowerOfFortuneMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :tower_of_fortunes, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, foreing_key: true, unique: true

      t.integer :played_games_count, default: 0
      t.boolean :is_playing_game, default: false
      t.datetime :free_game_at, null: false
      t.jsonb :hash_game_info, default: {}

      t.timestamps null: false
    end
  end
end
