module TowerOfFortune

  class GameInfo

    attr_accessor :has_failed, :fail_count, :current_round, :won_final_round, 
                  :accumulated_rewards

    def initialize
      @has_failed = false
      @fail_count = 0
      @current_round = 1
      @won_final_round = false
      @accumulated_rewards = []
    end

    def to_hash
      {
        hasFailed: @has_failed,
        failCount: @fail_count,
        currentRound: @current_round,
        wonFinalRound: @won_final_round,
        accumulatedRewards: @accumulated_rewards.map(&:deep_symbolize_keys),
      }
    end

    def self.from_hash(h)
      game_info = GameInfo.new

      return game_info if h.nil? || h.empty?

      h = h.deep_symbolize_keys

      game_info.has_failed = h[:hasFailed] || false
      game_info.fail_count = h[:failCount] || 0
      game_info.current_round = h[:currentRound] || 1
      game_info.won_final_round = h[:wonFinalRound] || false
      game_info.accumulated_rewards = h[:accumulatedRewards] || []
      game_info
    end

    def to_json
      JSON.generate(self.to_hash)
    end

    def self.from_json(json)
      h = JSON.parse(json)
      from_hash(h)
    end
  end
end