module TowerOfFortune

  class TowerOfFortune < ActiveRecord::Base

    belongs_to :user

    validates_presence_of :free_game_at

    before_save :update_hashes

    def game_info
      return @game_info unless @game_info.nil?
      @game_info = GameInfo.from_hash(hash_game_info)
      @game_info
    end

    def game_info=(value)
      @game_info = value
    end

    def to_hash
      {
        playedGamesCount: played_games_count,
        isPlayingGame: is_playing_game,
        unixFreeGameAt: free_game_at.to_i,
        gameInfo: game_info&.to_hash || GameInfo.new.to_hash,
      }
    end

    private

    def update_hashes
      self[:hash_game_info] = @game_info.to_hash unless @game_info.nil?
    end
  end
end
