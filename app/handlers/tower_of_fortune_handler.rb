module TowerOfFortune
  class TowerOfFortuneHandler < RubyPitaya::HandlerBase

    def getFeatureConfig
      feature_config = @config['plugin/tower_of_fortune/feature']

      response = {
        code: StatusCodes::CODE_OK,
        data: feature_config
      }
    end

    def getTowerInfo
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid

      tower = TowerOfFortuneHelper.get_tower(user_id)
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)

      response = {
        code: StatusCodes::CODE_OK,
        data: tower_info
      }
    end

    def getRounds
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      rounds_config = @config['plugin/tower_of_fortune/rounds']

      rounds = []
      rounds_config.each do |round_config|
        round = round_config.except(:rewards)
        round[:numberOfRewards] = round_config[:rewards].size

        rounds << round
      end

      tower = TowerOfFortuneHelper.get_tower(user_id)
      game_info = tower.game_info.to_hash

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          rounds: rounds,
          gameInfo: game_info,
        }
      }
    end

    def startGame
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      cost_info = @config['plugin/tower_of_fortune/cost_info']

      tower = TowerOfFortuneHelper.get_tower(user_id)
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)
      game_info = tower.game_info.to_hash

      cost = tower_info[:startGameCost]
      currency_code = tower_info[:startGameCurrencyCode]

      raise bll.error_not_enough_currency if !tower_info[:hasFreeGame] &&
                                          !bll.has_currency_to_start_new_game?(user_id, cost, currency_code, tower_info, game_info)

      tower.free_game_at = Time.now.utc + 24.hours
      tower.is_playing_game = true
      tower.played_games_count += 1
      tower.game_info = GameInfo.new

      ActiveRecord::Base.transaction do
        tower.save!
        bll.apply_cost_to_start_new_game(user_id, cost, currency_code, tower_info, game_info) unless tower_info[:hasFreeGame]
      end

      cost = 0 if tower_info[:hasFreeGame]

      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          cost: cost,
          currencyCode: currency_code,
          towerInfo: tower_info,
        }
      }
    end

    def chooseRoundOption
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      cost_info = @config['plugin/tower_of_fortune/cost_info']

      tower = TowerOfFortuneHelper.get_tower(user_id)
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)
      game_info = tower.game_info.to_hash

      raise error('You are not on a game to choose an option') unless tower.is_playing_game?
      raise error('You already won the final round, collect your prizes') if tower.game_info.won_final_round
      raise error('You already fail, choose give up or continue') if tower.game_info.has_failed

      rounds = @config['plugin/tower_of_fortune/rounds']

      current_round = rounds[tower.game_info.current_round - 1]
      rewards = current_round[:rewards].map(&:symbolize_keys)

      chosen_reward_index = bll.choose_reward_index(user_id, rewards, tower_info, game_info)
      chosen_reward = rewards[chosen_reward_index]

      other_rewards = rewards.clone
      other_rewards.delete_at(chosen_reward_index)

      chosen_reward = bll.get_correct_round_reward(user_id, chosen_reward, tower_info, game_info)

      if bll.has_lost_round?(user_id, chosen_reward, tower_info, game_info)
        tower.game_info.has_failed = true
        tower.game_info.fail_count += 1
        tower.save!

        tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)
        game_info = tower.game_info.to_hash

        return response = {
          code: StatusCodes::CODE_OK,
          data: {
            towerInfo: tower_info,
            gameInfo: game_info,
            roundRewardInfo: {
              chosenReward: chosen_reward,
              otherRewards: other_rewards,
            }
          }
        }
      end

      reward_accumulated = tower.game_info.accumulated_rewards.select{|reward| reward.except(:amount) == chosen_reward.except(:amount)}.first

      if reward_accumulated.nil?
        tower.game_info.accumulated_rewards << chosen_reward
      else
        reward_accumulated[:amount] += chosen_reward[:amount]
      end

      is_final_round = tower.game_info.current_round >= rounds.size

      if is_final_round
        tower.game_info.won_final_round = true
      else
        tower.game_info.current_round += 1
      end

      tower.save!
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)
      game_info = tower.game_info.to_hash

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          towerInfo: tower_info,
          gameInfo: game_info,
          roundRewardInfo: {
            chosenReward: chosen_reward,
            otherRewards: other_rewards,
          }
        }
      }
    end

    def collectRewards
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      tower = TowerOfFortuneHelper.get_tower(user_id)
      game_info = tower.game_info.to_hash

      raise error('You are not on a game to collect your rewards') unless tower.is_playing_game?
      raise error('You have failed, continue the game to collect your rewards') if tower.game_info.has_failed

      tower.played_games_count += 1
      tower.is_playing_game = false
      tower.free_game_at = Time.now.utc + 24.hours
      tower.game_info = GameInfo.new
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)

      applied_rewards = []
      ActiveRecord::Base.transaction do
        bll.before_claim_rewards(user_id)

        game_info[:accumulatedRewards].each do |reward|
          applied_rewards += bll.claim_reward(user_id, reward, tower_info, game_info)
        end

        bll.after_claim_rewards(user_id)

        tower.save!
      end

      game_info[:accumulatedRewards] = []

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          towerInfo: tower_info,
          gameInfo: game_info,
          rewards: applied_rewards
        }
      }
    end

    def giveUpAfterFail
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      tower = TowerOfFortuneHelper.get_tower(user_id)
      game_info = tower.game_info.to_hash

      raise error('You are not on a game to give up') unless tower.is_playing_game?
      raise error('You have not failed to give up') unless tower.game_info.has_failed

      tower.played_games_count += 1
      tower.is_playing_game = false
      tower.free_game_at = Time.now.utc + 24.hours
      tower.game_info = GameInfo.new
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)

      bll.gaveUp(user_id, tower_info, game_info)

      tower.save!

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          towerInfo: tower_info,
          gameInfo: game_info.to_hash,
        }
      }
    end

    def continueGameAfterFail
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      cost_info = @config['plugin/tower_of_fortune/cost_info']

      tower = TowerOfFortuneHelper.get_tower(user_id)
      tower_info = TowerOfFortuneHelper.get_tower_info(@config, tower)
      game_info = tower.game_info.to_hash

      raise error('You are not on a game to continue it') unless tower.is_playing_game?
      raise error('You have not failed to continue a game') unless tower.game_info.has_failed

      continue_cost_index = tower.game_info.fail_count - 1
      continue_cost_index = [continue_cost_index, cost_info[:continueGameCost].size].min

      cost = cost_info[:continueGameCost][continue_cost_index]
      currency_code = cost_info[:continueGameCurrencyCode]

      raise bll.error_not_enough_currency unless bll.has_currency_to_continue_game_after_fail?(user_id, cost, currency_code, tower_info, game_info)

      tower.game_info.has_failed = false

      ActiveRecord::Base.transaction do
        bll.apply_cost_to_continue_game_after_fail(user_id, cost, currency_code, tower_info, game_info)
        tower.save!
      end

      game_info = tower.game_info.to_hash

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          cost: cost,
          currencyCode: currency_code,
          towerInfo: tower_info,
          gameInfo: game_info.to_hash,
        }
      }
    end

    def getGameInfo
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      tower = TowerOfFortuneHelper.get_tower(user_id)

      response = {
        code: StatusCodes::CODE_OK,
        data: tower.game_info.to_hash
      }
    end

    def getCurrentRoundInfo
      feature_config = @config['plugin/tower_of_fortune/feature']
      return response_feature_not_enabled() unless feature_config[:enabled]

      user_id = @session.uid
      rounds = @config['plugin/tower_of_fortune/rounds']

      tower = TowerOfFortuneHelper.get_tower(user_id)

      raise error('You are not on a game to get current round info') unless tower.is_playing_game?
      raise error('You have failed, continue game to get current round info') if tower.game_info.has_failed

      current_round = rounds[tower.game_info.current_round - 1]

      round_info = current_round.except(:rewards)
      round_info[:numberOfRewards] = current_round[:rewards].size

      response = {
        code: StatusCodes::CODE_OK,
        data: round_info
      }
    end

    private

    def bll
      @objects[:bll]
    end

    def response_success(data)
      response = {
        code: StatusCodes::CODE_OK,
        data: data,
      }
    end

    def error(message, code = nil)
      code ||= StatusCodes::CODE_ERROR
      return RubyPitaya::RouteError.new(code, message)
    end

    def response_feature_not_enabled
      response = {
        code: StatusCodes::CODE_OK,
        message: 'Tower of Fortune is not enabled',
        data: {},
      }
    end
  end
end
