module TowerOfFortune

  class TowerOfFortuneHelper

    def self.get_feature_config(config)
      config['plugin/tower_of_fortune/feature']
    end

    def self.get_tower(user_id)
      tower = TowerOfFortune.find_by_user_id(user_id)
      tower ||= TowerOfFortune.new(user_id: user_id, free_game_at: Time.now.utc)
      tower
    end

    def self.get_tower_info_without_tower(user_id, config)
      tower = get_tower(user_id)
      get_tower_info(config, tower)
    end

    def self.get_tower_info(config, tower)
      cost_info = config['plugin/tower_of_fortune/cost_info']

      continue_cost_index = tower.game_info.fail_count - 1
      continue_cost_index = [continue_cost_index, cost_info[:continueGameCost].size].min
      continue_cost_index = [0, continue_cost_index].max
      continue_cost = cost_info[:continueGameCost][continue_cost_index]

      tower_info = {
        playedGamesCount: tower.played_games_count,
        isPlayingGame: tower.is_playing_game,
        hasFreeGame: tower.free_game_at.utc <= Time.now.utc,
        unixFreeGameAt: tower.free_game_at.to_i,
        startGameCost: cost_info[:startGameCost],
        continueGameCost: continue_cost,
        startGameCurrencyCode: cost_info[:startGameCurrencyCode],
        continueGameCurrencyCode: cost_info[:continueGameCurrencyCode],
      }
    end
  end
end